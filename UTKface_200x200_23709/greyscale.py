import cv2
import glob
import os


inputFolder = "dataset01"
os.mkdir("Grey01")
i=0
for img in glob.glob(inputFolder + "/*.png"):
    image=cv2.imread(img)
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    i += 1
    cv2.imwrite("Grey01/image%04i.jpg" % i, gray_image)
    cv2.waitKey(0)
cv2.destroyAllWindows()