import cv2
import glob
import os

inputFolder="negatives01"
os.mkdir("negative_data")
i=0
for img in glob.glob(inputFolder + "/*.jpg"):
    image=cv2.imread(img)
    imgResize= cv2.resize(image,(128,128))
    cv2.imwrite("negative_data/image%04i.jpg" %i, imgResize)
    i+=1
    cv2.waitKey(30)
cv2.destroyAllWindows()